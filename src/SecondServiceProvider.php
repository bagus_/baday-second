<?php namespace Baday\Second;

use Illuminate\Support\ServiceProvider;

class SecondServiceProvider extends ServiceProvider {
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function boot(){
		require __DIR__ . '/http/routes.php';
	}
	public function register()
	{
		$this->app->bind('todo', function($app){
			return new Todo;
		});
 
		//$this->registerHtmlBuilder();

		//$this->registerFormBuilder();

		//$this->app->alias('html', 'Illuminate\Html\HtmlBuilder');
		//$this->app->alias('form', 'Illuminate\Html\FormBuilder');
		// $this->app->alias('Second', 'Baday\Second\SecondBuilder');
 
		$this->loadViewsFrom(__DIR__.'/../views', 'view');

		//
		$this->publishes([
				__DIR__.'/migrations/2015_11_05_000000_create_second_todo_table.php' => base_path('database/migrations/2015_11_05_000000_create_second_todo_table.php')
			]);
		$this->publishes([
				__DIR__.'/../Assets/' => public_path('Assets/'),
			], 'public');
			
	}

}
