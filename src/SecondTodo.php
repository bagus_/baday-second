<?php

namespace Baday\Second;


use illuminate\Database\Eloquent\Model;

class SecondTodo extends Model{

	protected $table = "SecondTodo";

	protected $filable = ['id', 'nama', 'tanggal lahir', 'alamat', 'telepon'];
}