# Custom Package Laravel 5 #

## package direktori dan pembuatan file ##

buat folder baru bernama **packages** pada direktori root project, selanjutnya arahkan ke direktori tersebut kemudian buatlah struktur direktori sebagai berikut 


```
#!dir

packages/
        nama-author/
                   nama-package/
                               src/
                                  Asset/
                                  Http/
                                      Controller/
                                  Migrations/
                                  Model/
                                  Views/
```

### composer ###
buat file composer dengan menjalankan perintah dibawah ini pada terminal


```
#!cmd

composer init
```

setelah menjalankan perintah tersebut pada terminal, akan diminta untuk memasukan deskripsi mengenai package yang akan dibuat.

tambahkan pada file composer tersebut kode dibawah ini


```
#!php

"autoload": {
        "psr-4": {
            "nama-author\\nama-package\\": "src/"
        }
    },
```
## provider ##

pada folder src buatlah sebuah file baru dengan nama 


```
#!file

(nama-file)ServiceProvider.php
```
pada file tersebut tambahkan kode berikut


```
#!php

<?php

namespace nama-author\nama-package;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

```

## migration ##
pada folder "*migrations*" buat file baru dengan nama "*2015_11_05_000000_create_table_baru*", file tersebut bertujuan untuk membuat struktur tabel yang akan digunakan, pada file  tersebut yang saya buat struktur tabelnya adalah sebagai berikut :

```
#!php

<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('Tables', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('nama');
            $table->date('tanggallahir');
            $table->text('alamat');
            $table->string('telepon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('Tables');
    }
}

```

## MVC ( Modal-View-Controller )##
### - Model ###
pada direktori ""*/src*" buat sebuahfile  model baru untuk tabel sudah dibuat 

```
#!file

(nama-model).php
```
tambahkan kode untuk model tersebut, kode yang saya buat adalah berikut 

```
#!php

<?php

namespace nama-author\nama-package;


use illuminate\Database\Eloquent\Model;

class TabelModel extends Model{

	protected $table = "Tables";

	protected $filable = ['id', 'nama', 'tanggal lahir', 'alamat', 'telepon'];
}
```
### -View ###

untuk menampilkan isi dari tabel yang dibuat maka perlu dibuat view, maka pada folder "*/views*", 

```
#!file

(nama-view).blade.php
```
tambahkan kode untuk view tersebut, kode ynag sudah saya buat adalah sebagai berikut


```
#!html

<h1> View Tabel custom package </h1>

<table style="border: 1px solid black">
	<thead>
	<th>ID</th>
	<th>Nama</th>
	<th>Alamat</th>
	<th>Tanggal</th>
	<th>No Telepon</th>
	</thead>
	<tbody>
	@foreach ($result as $list)
	<tr style="border: 1px solid black">
		<td style="border: 1px solid black">{{$list->id}}</td>
		<td style="border: 1px solid black">{{$list->nama}}</td>
		<td style="border: 1px solid black">{{$list->alamat}}</td>
		<td style="border: 1px solid black">{{$list->tanggal_lahir}}</td>
		<td style="border: 1px solid black">{{$list->telepon}}</td>
	</tr>
	@endforeach
	</tbody>

</table>


```

### - Controller ###

buat sebuah file controller baru , pada folder "*/Http/Controller/*"

```
#!file

(nama-controler)Controller.php
```
tambahkan kode pada file controller yang dibuat, pada file yang saya buat kodenya adalah sebagai berikut

```
#!php
<?php namespace nama-author\nama-package\Http\Controller;

use nama-author\nama-package\Model;
use App\Http\Controllers\Controller;
class (nama-controller)Controller extends Controller{

	public function example(){

		
		$result = result::all();
		 return view('view::(nama-view) ', compact('result'));
	}

}

```

## Routes ##
buat file Routes.php baru di direktori "*/Http*", dan isi file yang saya buat adalah berikut:

```
#!php

<?php

Route::get('CustomPackage', 'nama-author\nama-package\Http\(nama-controller)Controller@example');
```
## Publish ##

kembali lagi ke file *ServiceProvider* untuk bisa mengakses file yang ada di package yang dibuat maka pada file ServceProvider , tambahkan kode untuk mempublish file-file tersebut
pada file yang saya buat saya menambahkan kode berikut

```
#!php

public function boot(){
	require __DIR__ . '/http/routes.php';
}
public function register(){
	$this->loadViewsFrom(__DIR__.'/views', 'view');
	$this->publishes([__DIR__.'/migrations/2015_11_05_000000_create_second_todo_table.php' => base_path('database/migrations/2015_11_05_000000_create_second_todo_table.php')
	]);
	$this->publishes([ __DIR__.'/Assets/' => public_path('Assets/')], 'public');
	}

```
untu info lebih lanjut mengenai cara untuk mempublish View, Migration dll, [Silahkan Klik disini](http://laravel.com/docs/5.0/packages#publishing-file-groups)

# PENGGUNAAN #

untuk penggunaan di project baru, tambahkan pada file composer.json pada root project kode berikut,
keterangan : 
* nama-package : adalah nama package yang d buat di composer.json milik package yang sudah dibuat sebelumnya
* url packgage : copykan  URL tempat file package di upload 

```
#!

"require": {
		
		"(nama-package)": "dev-master"
	},
	"repositories": [
        {
            
	            "type": "git",
	            "url": "Url package"

        }
    ],
```


pada file *config/app* tambahkan kode berikut



```
#!php

'providers' => [
........
	//untuk Laravel 5.0.*
        'nama-author\nama-package\(nama-provider)ServiceProvider',
        
        //untuk Laravel 5.1*
        nama-author\nama-package\(nama-provider)ServiceProvider::class,
........
	],

'aliases' => [
........
        //untuk Laravel 5.0.*
	'result' => 'nama-author\nama-package\(nama-model)',
        'example' = 'nama-author\nama-package\(nama-controller)Controller',

        //untuk Laravel 5.1.*
	'result' => nama-author\nama-package\(nama-model)::class,
        'example' = nama-author\nama-package\(nama-controller)Controller::class,
........
	],
```


# Testing #
buka terminal di root project laravel, kemudian jalankan perintah 
```
#!composer

php artisan vendor:publish
```

```
#!cmd

php artisan migrate
```

buka pada browser jalankan project yang dibuat


```
#!cmd

http://localhost/(nama-project)/public/CustomPackage
```



**Finish !!**